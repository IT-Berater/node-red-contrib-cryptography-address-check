# node-red-contrib-cryptography-address-check

[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

This Node check Crypto Addresses.

Sample Flow:

![image](http://blog.wenzlaff.de/wp-content/uploads/2021/03/wenzlaff.de-2021-03-16-um-18.49.14.png)

![image](http://blog.wenzlaff.de/wp-content/uploads/2021/03/node-red-address-check-flow.png)

Dashboard:
Valid:
![image](http://blog.wenzlaff.de/wp-content/uploads/2021/03/wenzlaff.de-2021-03-16-um-18.47.20.png)
Wrong:
![image](http://blog.wenzlaff.de/wp-content/uploads/2021/03/wenzlaff.de-2021-03-16-um-18.49.43.png)

<p>This node take a bitcoin address and check it. Returns true if the address (string) is a valid wallet address for the crypto currency specified.</p>
    <p>Network Type - Optional. Use (prod) (default) to enforce standard address, (testnet) to enforce testnet address and (both) to enforce nothing.</p>
    <p>Supported crypto currencies:</p>
    <pre>
    Auroracoin/AUR, 'auroracoin' or 'AUR'
    Bankex/BKX, 'bankex' or 'BKX'
    BeaverCoin/BVC, 'beavercoin' or 'BVC'
    Biocoin/BIO, 'biocoin' or 'BIO'
    Bitcoin/BTC, 'bitcoin' or 'BTC' (default)
    BitcoinCash/BCH, 'bitcoincash' or 'BCH'
    BitcoinGold/BTG, 'bitcoingold' or 'BTG'
    BitcoinPrivate/BTCP, 'bitcoinprivate' or 'BTCP'
    BitcoinZ/BTCZ, 'bitcoinz' or 'BTCZ'
    Callisto/CLO, 'callisto' or 'CLO'
    Dash, 'dash' or 'DASH'
    Decred/DCR, 'decred' or 'DCR'
    Digibyte/DGB, 'digibyte' or 'DGB'
    Dogecoin/DOGE, 'dogecoin' or 'DOGE'
    Ethereum/ETH, 'ethereum' or 'ETH'
    EthereumClassic/ETH, 'ethereumclassic' or 'ETC'
    EthereumZero/ETZ, 'etherzero' or 'ETZ'
    Freicoin/FRC, 'freicoin' or 'FRC'
    Garlicoin/GRLC, 'garlicoin' or 'GRLC'
    Hush/HUSH, 'hush' or 'HUSH'
    Komodo/KMD, 'komodo' or 'KMD'
    Litecoin/LTC, 'litecoin' or 'LTC'
    Megacoin/MEC, 'megacoin' or 'MEC'
    Monero/XMR, 'monero' or 'XMR'
    Namecoin/NMC, 'namecoin' or 'NMC'
    Nano/NANO, 'nano' or 'NANO'
    NEO/NEO, 'NEO' or 'NEO'
    NeoGas/GAS, 'neogas' or 'GAS'
    Peercoin/PPCoin/PPC, 'peercoin' or 'PPC'
    Primecoin/XPM, 'primecoin' or 'XPM'
    Protoshares/PTS, 'protoshares' or 'PTS'
    Qtum/QTUM, 'qtum' or 'QTUM'
    Raiblocks/XRB, 'raiblocks' or 'XRB'
    Ripple/XRP, 'ripple' or 'XRP'
    Snowgem/SNG, 'snowgem' or 'SNG'
    Vertcoin/VTC, 'vertcoin' or 'VTC'
    Votecoin/VTC, 'votecoin' or 'VOT'
    Zcash/ZEC, 'zcash' or 'ZEC'
    Zclassic/ZCL, 'zclassic' or 'ZCL'
    ZenCash/ZEN, 'zencash' or 'ZEN'
    </pre>
