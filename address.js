module.exports = function (RED) {
  const NodeName = 'addresscheck'

  function AddressCheckNode (config) {
    RED.nodes.createNode(this, config)

    this.currency = config.currency
    this.networktype = config.networktype

    const node = this

    this.status({ fill: 'green', shape: 'dot', text: 'currency: ' + node.currency + ' networkType: ' + node.networktype })

    const WAValidator = require('wallet-address-validator')

    function validatePublicAddress (publicAddress) {
      return WAValidator.validate(publicAddress, node.currency, node.networktype)
    }

    this.on('input', function (msg) {
      msg.payload = validatePublicAddress(msg.payload)
      node.send(msg)
    })
  }
  RED.nodes.registerType(NodeName, AddressCheckNode)
}
